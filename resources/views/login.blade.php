<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Redilogic Components</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
    <style>
        .login-wrapper {
            margin-top: 13%;
            max-width: 600px;   
        }
    </style>
</head>
<body>
    <div class="login-wrapper container">
        <div class="row align-items-center justify-content-center">
            @component('componentes.card', ["md" => "6", "class" => "login-panel"])
                @slot('header')
                    <h3>Login</h3>
                @endslot
                @component('componentes.form')
                    @slot('inputs', [
                        ['nombre' => 'Usuario'],
                        ['nombre' => 'Contraseña', 'tipo' => 'password']
                    ])
                @endcomponent
            @endcomponent
        </div>
    </div>
    <script src="{{asset('js/app.js')}}"></script>
</body>
</html>