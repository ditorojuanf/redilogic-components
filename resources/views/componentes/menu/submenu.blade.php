{{--  Menu>Submenu Componente, especificamente elaborado para el componente Menu  --}}
{{--  

    Este componente es solo un envolvente para el componente Menu>Link, le otorga 
    la accion al submenu.

    props:
        -id:  identificador del componente

  --}}
  
<div class="collapse {{$id}} menu-submenu" id="{{$id}}">
    <div class="col-12">
        {{$slot}}
    </div>
</div>
