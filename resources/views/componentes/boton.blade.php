{{-- Button Component --}}
{{--
props:
    -type :
        - submit
        - danger
        - success
        - primary 
        - warning

    -size: 
        - lg
        - sm
        - block    

    -id: identificador

    -target: es el target(objetivo) del boton sigue los lineamientos de 'data-target' y 'data-toggle' de bootstrap   
 --}}
<?php $isSubmit = 'submit'; ?>
<button 
    @isset($type)
        @if($type == $isSubmit) 
        type="{{$type}}" 
        class="btn btn-secondary {{(! empty($size) ? 'btn-'.$size : '')}}"
        @else
        type="button" 
        @endif
    @endisset

    class="btn btn-{{$type or 'secondary'}} {{(! empty($size) ? 'btn-'.$size : '')}}" 



    @isset($id)
    id= "{{$id}}"
    @endisset

    @isset($target)
    data-toggle="{{$target}}"
    data-target="#{{$target}}"
    @endisset>

        {{$slot}}

</button>

