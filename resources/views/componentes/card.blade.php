{{--  NO ESTA TERMINADO NECESITA MEJORAS  --}}
<div class="card 
    col-sm-{{$sm or "12"}}
    @isset($md){{"col-md-".$md}}@endisset
    @empty($md){{"col-md"}}@endempty
    @isset($lg){{"col-lg-".$lg or $md or ""}}@endisset 
    @empty($lg){{"col-lg"}}@endempty
    {{$class or ""}} 
    m-2">
    @isset($header)
        <div class="card-header">
            {{$header}}
        </div>
    @endisset
    <div class="card-body">
        {{$slot}}
    </div>
    @isset($footer)
        <div class="card-footer">
            {{$footer}}
        </div>
    @endisset
</div>