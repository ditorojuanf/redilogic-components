function expandMenuWrapper(selectorDataTarget) {
    let selector = $('.menu-opcion-trigger');
    let submenuCollapsed = $('.menu-submenu-collapsed');

    selector.each(function (index, el) {
        $(el).attr('data-target', selectorDataTarget[index]);
    });
    $('.menu-collapse-btn').find('i').html('keyboard_arrow_left');
    $('.menu-wrapper').attr('data-collapsed', 'false');
    $('.menu-wrapper').removeClass('menu-wrapper-collapsed');
    $('.menu-wrapper').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function (e) {
        $('.menu-collapse-btn').find('i').html('keyboard_arrow_left');
        $('.menu-opcion-trigger').removeClass('menu-collapsed');
        selector.find('span.menu-texto').show();
        selector.find('span.menu-expand').show();
        $('.menu-footer').find('span.menu-texto').show();  
    });

    submenuCollapsed.addClass('d-none hide');

    selector.attr('data-toggle', 'collapse');
    selector.removeClass('dropright');
    selector.addClass('collapsed');
    $('.menu-link').removeClass('dropdown-item');

}

function collapseMenuWrapper(selectorDataTarget) {
    let selector = $('.menu-opcion-trigger');
    let submenuCollapsed = $('.menu-submenu-collapsed');

    selector.find('span.menu-texto').hide();
    selector.find('span.menu-expand').hide();
    selector.each(function (index, el) {
        $(el).attr('data-target', selectorDataTarget[index] + "Collapsed");
    });
    selectorDataTarget.forEach(element => {
        $(element).collapse('hide');
    });
    
    $('.menu-footer').find('span.menu-texto').hide();

    $('.menu-wrapper').addClass('menu-wrapper-collapsed');
    $('.menu-wrapper').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function (e) {
        $('.menu-collapse-btn').find('i').html('keyboard_arrow_right');
    });
    $('.menu-opcion-trigger').addClass('menu-collapsed');
    $('.menu-wrapper').attr('data-collapsed', 'true');
    submenuCollapsed.removeClass('d-none hide');

    selector.attr('data-toggle', 'dropdown');
    selector.addClass('dropright');
    selector.removeClass('collapsed');
    $('.menu-link').addClass('dropdown-item');
}

// START MENU COMPONENTE 

$('.menu-wrapper').ready(function () {
    let collapseBtn = $('.menu-collapse-btn');
    let wrapperWidth = $('.menu-wrapper').width();
    let submenuCollapsed = $('.menu-submenu-collapsed');



    /*
     *--------------------------------------------------- 
     *  Recalculamos el tamaño del wrapper abierto 
     *  cada vez que se cambia el tamaño de la pantalla
     *---------------------------------------------------
     */



    $(window).resize(function(){
        let hadClass = $('.menu-wrapper').hasClass('menu-wrapper-collapsed');
        if($('.menu-wrapper').attr('style')) {
            $('.menu-wrapper').removeAttr('style');
        }
        if( hadClass ) {
            $('.menu-wrapper').removeClass('menu-wrapper-collapsed');
        }
        wrapperWidth = $('.menu-wrapper').width();
        if(hadClass) $('.menu-wrapper').addClass('menu-wrapper-collapsed');
    });

    let selector = $('.menu-opcion-trigger');
    let selectorDataTarget = [];
    
    // recolectamos todos los data-target
    selector.each(function (index) {
        selectorDataTarget.push($(this).attr('data-target'));
    });
    
    //  manejamos el collapse del menu
    collapseBtn.click(function () {
        if($('.menu-wrapper').attr('data-collapsed') == "false"){
            collapseMenuWrapper(selectorDataTarget);
        }

        else {
            expandMenuWrapper(selectorDataTarget);
        }
    });

     /*--------------------------------------------------- 
     *
     *                 SUB-MENU COLAPSADO
     *  
     *---------------------------------------------------
     */

    $('.menu-opcion-trigger')
    .mouseover( function (e) {
        // console.log('entro', $(this));
        let menuOpcion = $(this).hasClass('menu-collapsed') ? $(this)  : null;
        if(!menuOpcion) return;
        console.log('entro');
        let menuObjetivo = menuOpcion.attr('data-target');

        $(this).find('.menu-submenu-collapsed').addClass('show');
        setTimeout(() => {
            if($(this).find('.menu-submenu-collapsed').hasClass('show'))
                $(this).find('.menu-submenu-collapsed').addClass('submenu-show');
        }, 100);
    })
    .mouseleave( function (e) {
        let menuOpcion = $(this).hasClass('menu-collapsed') ? $(this) : null;
        if(!menuOpcion) return;
        console.log('salio');
        console.log(menuOpcion);

        let menuObjetivo = menuOpcion.attr('data-target');

        $(this).find('.menu-submenu-collapsed').removeClass('show');
        if($(this).find('.menu-submenu-collapsed').hasClass('submenu-show')){
            $(this).find('.menu-submenu-collapsed').removeClass('submenu-show');
        }
    });
});

// END MENU COMPONENTE