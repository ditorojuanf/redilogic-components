<div class="dropdown-menu {{$id}} menu-submenu-collapsed hide d-none" id="{{$id}}Collapsed">
    <div class="col-12">
        <h4 class="dropdown-header">{{$titulo}}</h4>
        {{$slot}}
    </div>
</div>