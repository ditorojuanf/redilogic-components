{{-- Modal Componente --}}
{{--
props:
    -class: pass a class to add to modal

    -id: can be whatever you want it to be, 
        remember if you want to launch this 
        modal you have to pass this modal id
        as 'target' in a button

    -hidden: sets the modal hidden by default
    
slots: 
    -title: modal title

    -default: whatever you put inside @component directive is going to be the default slot, this is the modal body
    
    -footer: modal footer
 --}}
<div class="modal {{$class or ''}}"
     id="{{$id or 'modal'}}"
     @isset($hidden)
     aria-hidden = "true"
     @endisset
>
<div class="modal-dialog" role="document">
    <div class="modal-content">
            @isset($title)
                    <div class="modal-header">
                        <h5 class="modal-title"> {{$title}} </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
            @endisset
            <div class="modal-body">
                {{$slot}}
            </div>
            @isset($footer)
            <div class="modal-footer">
                {{$footer}}
            </div>
            @endisset
            </div>
        </div>
</div>
