{{--  Menu>Submenu>Link Componente, elaborado especificamente para el componente Submenu  --}}
{{--  
    Este componente es el link de cada una de las opciones del submenu.

    props:
        -url: Direccion absoluta a la que se redireccionara al usuario
        -texto: Texto a mostrar.
  --}}
  
<div class="row"> 
    <a href="http://{{$url}}" class="col menu-link py-1"> {{$texto}}</a>
</div>