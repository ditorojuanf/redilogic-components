{{--  Menu Componente  --}}
{{--  POR HACER: hacer responsive para mobile  --}}
{{--  
    Este componente muestra el menu de la aplicacion, es importante declarar
    un 'offset-sm-3 offset-lg-2' en el componente que acompañe a este para que
    se cumpla el layout completamente, tambien es importante destacar que este
    componente debe de ir dentro de lo siguiente: 
        <div class="container">
            <div class="row">
                //componente aqui//
                //otra cosa aqui, recordar que debe contener las siguientes clases 'offset-sm-3 offset-lg-2'//
            </div>
        </div>    
    

    Dependencias:
        - Bootstrap 4 (css y js)
        - Jquery >3.1
        - Material Icons (https://material.io/icons)

    -slot : en slot se debe pasar lo que se quiera tener como logo 
  --}}


<div class=" col-sm-2 col-lg-1 menu-wrapper" data-collapsed="false">
    <div class="menu-logo py-2">
        <h3><strong>R</strong></h3>
    </div>
    @component('componentes.menu.opcion', 
    ['icon' => 'assignment', 
    'texto' => 'Logistica', 
    'collapsable' => 'true',
    'objetivo' => 'logisticaCollapse'])
        @slot('links', [
            ['texto' => 'hola', 'url' => 'localhost:8000/asd'],
            ['texto' => 'holass', 'url' => 'localhost:8000/asasd']
        ])
    @endcomponent
    @component('componentes.menu.opcion', 
    ['icon' => 'attach_money', 
    'texto' => 'Factura', 
    'collapsable' => 'true',
    'objetivo' => 'logistica2Collapse'])
        @slot('links', [
            ['texto' => 'hola', 'url' => 'localhost:8000/asd'],
            ['texto' => 'holass', 'url' => 'localhost:8000/asasd']
        ])
    @endcomponent
    @component('componentes.menu.opcion', 
    ['icon' => 'assignment', 
    'texto' => 'Logistica', 
    'collapsable' => 'true',
    'objetivo' => 'logistica3Collapse'])
        @slot('links', [
            ['texto' => 'hola', 'url' => 'localhost:8000/asd'],
            ['texto' => 'holass', 'url' => 'localhost:8000/asasd']
        ])
    @endcomponent
    <div class="row">
        <div role="button" class="col-12 menu-footer mx-1 menu-collapse-btn">
            <div class="btn col-3">
                <i class="material-icons">keyboard_arrow_left</i>
            </div>
            <span class="col-7 menu-texto">Esconder</span>
        </div>
    </div>
</div>
