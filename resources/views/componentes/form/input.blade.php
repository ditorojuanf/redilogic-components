<div class="form-group">

    <label for="{{camel_case($name)}}">{{$name}}</label>
    
    <input type="{{$type or 'text'}}" class="form-control" id="{{camel_case($name)}}" placeholder="Ingrese {{$name}}">

    @isset($message)
        <small id="{{camel_case($name)}}" class="form-text text-muted">{{$message}}</small>
    @endisset
</div>