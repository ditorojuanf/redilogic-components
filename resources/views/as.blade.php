<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Redilogic Components</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
</head>
<body>
{{--  <div class="container">  --}}
    {{--  <div class="row">  --}}
    
    {{--  </div>  --}}
{{--  </div>  --}}
<div class="container app-wrapper">
    <div class="row app-content">
        @component('componentes.menu')
        @endcomponent
        <div class="col p-4 ml-4">
            <div class="row">
                @component('componentes.card')
                holahola 
                @endcomponent
                @component('componentes.card')
                holahola 
                @endcomponent
                @component('componentes.card')
                holahola 
                @endcomponent
                @component('componentes.card')
                holahola 
                @endcomponent
            </div>
        </div>
    </div>
</div>

    
<script src="{{asset('js/app.js')}}"></script>
<script src="{{asset('/js/componentes/menu.js')}}"></script>
</body>
</html>