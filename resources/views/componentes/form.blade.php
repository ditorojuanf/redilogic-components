{{--  POR REALIZAR: 
        - FALTAN TODOS LOS CAMPOS PARA VALIDAR LA INFORMACION DEL FORMULARIO 

--}}

<form>
    @foreach($inputs as $input)
        @component('componentes.form.input')
            @slot('name', $input["nombre"])
            @isset($input["tipo"])
                @slot('type', $input["tipo"])
            @endisset 
        @endcomponent
    @endforeach
    @isset($boton)
        {{$boton}}
    @endisset
    @empty($boton)
        @component('componentes.boton')
            @slot('type', 'submit')
            @slot('size', 'block')
            Enviar
        @endcomponent
    @endempty
</form>