{{--  Menu Opcion Componente, especificamente elaborado para el componente menu  --}}
{{--  
props:  
    - icon: nombre del icono. proveniente de https://material.io/icons/
    - text
    - collapsable: si es verdadero 
    - objetivo: en caso de que esta opcion sea collapsable, es decir, tiene un submenu
      el 'objetivo' sera la el ID del mismo, esto para implementacion de los 'collapsable' 
      de bootstrap

slot:
    - links: 
        links debe ser un array asociativo, con dos parametros 'texto' y 'url'
            'texto' : sera el texto que aparecera en el link
            'url' : sera la direccion a la que se redireccionara al usuario. 
                    IMPORTANTE: el url debe ser la direccin absoluta. 
                    Ejemplo: 'localhost:8000/asasd' 
  --}}
  
<div class="row">
    <div class="btn col-12 menu-opcion" 
         role="button">
         
         <div class="row">
            <div class="col-12 ">
                <div class="row py-3 px-4 menu-opcion-trigger"
                 @isset($collapsable)
                    data-toggle="collapse" 
                    data-target="#{{$objetivo}}" 
                    aria-expanded="false" 
                    aria-controls="{{$objetivo}}"
                @endisset
                >
                
                    <span class="col-2 menu-icon"><i class="material-icons">{{$icon}}</i></span>
                    <span class="col-6 menu-texto">{{$texto}}</span>
                    @isset($collapsable)
                        <span class="col-4 menu-icon menu-expand"><i class="material-icons d-inline ml-2 mt-4">keyboard_arrow_down</i></span>
                    @endisset
                    
                    @isset($collapsable)
                        @component('componentes.menu.submenu-collapsed')
                            @slot('id')
                                {{$objetivo}}
                            @endslot
                            @slot('titulo')
                                {{$texto}}
                            @endslot
                            @isset($links)
                                @foreach ($links as $link)
                                    @component('componentes.menu.link')
                                        @slot('texto', $link['texto'])
                                        @slot('url', $link['url']);
                                    @endcomponent
                                @endforeach
                            @endisset
                        @endcomponent
                    @endisset
                </div>


         @isset($collapsable)
            @component('componentes.menu.submenu')
                @slot('id')
                    {{$objetivo}}
                @endslot
                @isset($links)
                    @foreach ($links as $link)
                        @component('componentes.menu.link')
                            @slot('texto', $link['texto'])
                            @slot('url', $link['url']);
                        @endcomponent
                    @endforeach
                @endisset
            @endcomponent
        @endisset
         </div>
        </div>
    </div>
</div>

{{--  @isset($collapsable)
    @component('componentes.menu.submenu')
        @slot('id')
            {{$objetivo}}
        @endslot
        @isset($links)
            @foreach ($links as $link)
                @component('componentes.menu.link')
		            @slot('texto', $link['texto'])
		            @slot('url', $link['url']);
	            @endcomponent
            @endforeach
        @endisset
    @endcomponent
@endisset  --}}
